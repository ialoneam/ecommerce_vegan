/*
https://tranquil-island-31366.herokuapp.com/
https://git.heroku.com/tranquil-island-31366.git
https://lit-refuge-59584.herokuapp.com/
*/
/* Require Dependencies
-------------------------------------------------------------*/
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const env = require('dotenv/config')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const app = express()
const auth = require('./routes/auth')
const localport = 4000;



/* Use packages, Handle JSON & Forms data
-------------------------------------------------------------*/
app.use(passport.initialize());
app.use(cors());
app.use(express.json());			
app.use(express.urlencoded({extended:true}));	



/* Connect Database
-------------------------------------------------------------*/
mongoose.connect(process.env.DB, 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Cloud database connection error"));
db.once("open", () => console.log(`Connected to MongoDB Atlas cloud`));



/* Middleware
-------------------------------------------------------------*/
app.use((req, res, next) => {
	console.log(`request method: ${req.method}`);
	console.log(`request headers: ${req.headers.authorization}`);
	console.log(`request body: ${req.body}`);
	next();
})

app.use((err, req, res, next) => {
	console.log(err)
	res.status(err.status).json({
		message: err.message })
})	



/* Routes
-------------------------------------------------------------*/
const adminRoutes = require('./routes/admin')
const productRoutes = require('./routes/product')
const userRoutes = require('./routes/user')
const orderRoutes = require('./routes/order')
const cartRoutes = require('./routes/cart')

app.use('/admin', adminRoutes);
app.use('/products', productRoutes);
app.use('/users', userRoutes);
app.use('/orders', orderRoutes);
app.use('/carts', cartRoutes);



/* Server Listening
-------------------------------------------------------------*/
const server = app.listen(process.env.PORT || localport, ()=>{
	const port = server.address().port;
	console.log(`Capstone Server is running at ${port}`);
})
