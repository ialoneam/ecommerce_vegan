const express = require('express')
const router = express.Router()
const User = require('../models/User')
const bcrypt = require('bcrypt')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const env = require('dotenv/config')

//require('./passport-setup')
router.use(cors())



/* Create JWT *
-------------------------------------------------------------*/
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin };
	return jwt.sign(data, process.env.SECRET, {});
}


/* Create JWT Postman*
-------------------------------------------------------------*/
module.exports.createAccessTokenPm = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin };
	return jwt.sign(data, process.env.SECRET, {});
}


/* Verify JWT, or User Authentication
-------------------------------------------------------------*/
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	console.log(`Token in auth.verify:`);
	console.log(token);

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, process.env.SECRET, (err, data) => {
			console.log(`Data in auth.verify():`);
			console.log(data);

			if(err) {return res.send({auth: "verify failed"})} 
			else {next()} })} 
	else {return res.status(403).json(
		{message: "Access denied."} )}
} 


/* Decrypt JWT
-------------------------------------------------------------*/
module.exports.decode = async (token) => {
	if(typeof token !== undefined || null || false) {
		token = token.slice(7, token.length);
		return jwt.verify(token, process.env.SECRET, (err, data) => {
			console.log(`Data in auth.decode():`);
			console.log(data);

			if(err) {return null} 
			else {return jwt.decode(token, {complete:true}).payload} })
					console.log(`Decoded in auth.decode:`);
					console.log(token);} 
	else {return null}
}


/* USER: Check If User Is Admin
-------------------------------------------------------------*/
module.exports.isAdmin = (req, res, next) => {
	const user = this.decode(req.headers.authorization);
	console.log(`Data in auth.isAdmin():`);
	console.log(user);

	if(user.isAdmin) { next() }
	else{console.log("Only an admin can perform this action.");
		return res.status(403).send(`Only an admin can perform this action.`) }
}


/* USER: Check If User Is Customer
-------------------------------------------------------------*/
module.exports.isCustomer = (req, res, next) => {
	const user = this.decode(req.headers.authorization);
	if(!user.isAdmin) { next() }
	else{return res.status(403).send(`Only a customer can perform this action.`) }
}




/* Login via JWT
-------------------------------------------------------------*/
router.post('/login', async (req,res, next) => {

	const user = await User.findOne({email: req.body.email})
	if(!user) return res.send('Invalid email')
	
	const passVerification = await bcrypt.compare(req.body.password, user.password)
	if(!passVerification) return res.send('Invalid password')

	const token = jwt.sign({_id: user._id}, process.env.SECRET)
	user.password = undefined;
	return res.json({body: 
		{
			user: user,
			token: token
		} 
	})
})


/* Login via Google
-------------------------------------------------------------*/
// router.post('/google/token', passport.authenticate('google-token', {session : false, scope : "email profile openid"}), function(req, res){
	
// 	let token = jwt.sign({id: req.user._id}, process.env.SECRET);
// 	return res.status(200).json({
// 		message: "login via google successful",
// 		data: {
// 			id: req.user.id,
// 			token: token
// 		}
// 	})
// })


/* Login via Facebook
-------------------------------------------------------------*/
// router.post('/facebook/token', passport.authenticate('facebook-token', {session: false}),
// 	function(req, res){
// 	let token = jwt.sign({id: req.user._id}, process.env.SECRET);
// 	return res.status(200).json({
// 		message: 'login via facebook successful',
// 		data: {
// 			id: req.user._id,
// 			token: token
// 		}
// 	})
// })


// module.exports = router;