const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')
const auth = require('./auth')



/* USER: Check If Email Exists ^
-------------------------------------------------------------*/
router.post("/checkEmail", async (req, res) => {
	userController.checkEmailExists(req.body)
	.then(resultFromController => res.send(resultFromController)); });


/* USER: Create/Register ^
-------------------------------------------------------------*/
router.post("/register", async (req, res) => {
	userController.createUser(req.body)
		.then(resultFromController => res.send(resultFromController)); });


/* USER: Login ^
-------------------------------------------------------------*/
router.post("/login", async (req, res) => {
	await userController.loginUser(req.body)
	.then(resultFromController => {
		res.send(resultFromController) }); });


/* USER: Details ^
-------------------------------------------------------------*/
router.get("/details", async (req, res) => {
	const user = await auth.decode(req.headers.authorization);
	userController.getProfile(user.id)
	.then(resultFromController => res.send(resultFromController)); });





/* USER: Get All *
-------------------------------------------------------------*/
router.get("/all", (req, res) => {
	userController.getUsers()
	.then(resultFromController => res.send(resultFromController));
});


/* USER: Get All Active *
-------------------------------------------------------------*/
router.get("/", auth.verify, auth.isAdmin, (req, res) => {
	userController.getActiveUsers()
	.then(resultFromController => res.send(resultFromController));
});


/* USER: Get User By ID *
-------------------------------------------------------------*/
router.get("/:userId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const userId = req.params.userId;
	userController.getProfile(user, userId)
	.then(resultFromController => res.send(resultFromController));
});

/* USER: Update By Put *
-------------------------------------------------------------*/
router.put("/:userId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.updateUser(user, req.body)
	.then(resultFromController => res.send(resultFromController));
})


/* USER: Update By Patch (Set User to Admin) *
-------------------------------------------------------------*/
router.patch("/:userId/setAsAdmin", auth.verify, auth.isAdmin, (req, res) => { 
	const user = auth.decode(req.headers.authorization);
	userController.updateUserToAdmin(user, req.params.userId)
	.then(resultFromController => res.send(resultFromController));
})


/* USER: Update By Patch (Set Back to User) *
-------------------------------------------------------------*/
router.patch("/:userId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.updateAdminToUser(user, req.params.userId)
	.then(resultFromController => res.send(resultFromController));
})


/* USER: Soft Delete (Archive) *
-------------------------------------------------------------*/
router.put("/archive/:userId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const userId = req.params.userId;
	userController.archiveUser(user, userId)
	.then(resultFromController => res.send(resultFromController));
})


/* USER: Hard Delete *
-------------------------------------------------------------*/
router.delete("/:userId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const userId = req.params.userId;
	userController.deleteUser(user, userId)
	.then(resultFromController => res.send(resultFromController));
})


/* USER: Export router *
-------------------------------------------------------------*/
module.exports = router;