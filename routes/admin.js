const express = require('express')
const router = express.Router()
const auth = require('./auth')
const jwt = require('jsonwebtoken')
const env = require('dotenv/config')



/* Home Admin Page *
-------------------------------------------------------------*/
router.get("/home", async (req) => {
	const user = req.headers.authorization;
	console.log(user);

	token = user.slice(7, user.length);
	console.log(token);

	return jwt.verify(token, process.env.SECRET, (err, data) => 
	{if(err) {return null} 
	else { decoded = jwt.decode(token, {complete:true}).payload;
			console.log(decoded);
		return decoded; } }) 
	
})


/* Home Admin Page PM *
-------------------------------------------------------------*/
router.get("/home/pm", async (req, res) => {
	const user = req.headers.authorization;
	console.log(user);

	token = user.slice(7, user.length);
	console.log(token);

	return jwt.verify(token, process.env.SECRET, (err, data) => 
	{if(err) {return null} 
	else { payload = jwt.decode(token, {complete:true}).payload;
			console.log(payload);
		return res.send(payload); } }) 
	
})


/* ADMIN: Export router *
-------------------------------------------------------------*/
module.exports = router;