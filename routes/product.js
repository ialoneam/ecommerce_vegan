const express = require("express")
const passport = require('passport')
const jwt = require('jsonwebtoken')
const productController = require('../controllers/product')
const auth = require('./auth')
const router = express.Router()



/* PRODUCT: Retrieve All Products *
-------------------------------------------------------------*/
router.get("/all", (req, res, next) => {
	// const user = auth.decode(req.headers.authorization);
	productController.getAllProducts()
	.then(resultFromController => res.send(resultFromController))
	.catch(next)
})


/* PRODUCT: Retrieve Active Products *
-------------------------------------------------------------*/
router.get("/", (req, res, next) => {
	productController.getActiveProducts()
	.then(resultFromController => res.send(resultFromController))
	.catch(next)
})


/* PRODUCT: Retrieve One Product By ID
-------------------------------------------------------------*/
router.get('/:productId', async (req, res, next) => {
	let productId = req.params.productId;
	productController.getProduct(productId)
	.then(resultFromController => res.send(resultFromController))
	.catch(next)
})


/* PRODUCT: Check If Product Exists *
-------------------------------------------------------------*/
router.post("/checkProduct", (req, res) => {
	productController.checkProductExists(req.body)
	.then(resultFromController => res.send(resultFromController));
})


/* PRODUCT: Create Product ^
-------------------------------------------------------------*/
router.post("/add", async (req, res) => {
	// var user = auth.decode(req.headers.authorization);

	let isExisting = await productController.checkProductExists(req.body);
		if(isExisting) 
		{res.send(`ERROR! Product ${req.body.name} is already existing.`)} 
		else {productController.addProduct(req.body)
			.then(resultFromController => res.send(resultFromController) )}}
)


/* PRODUCT: Update Product By Id
-------------------------------------------------------------*/
router.put("/:productId", async (req, res) => {
	// const user = auth.decode(req.headers.authorization);
	const productId = req.params.productId;
	productController.updateProduct(productId, req.body)
	.then(resultFromController => res.send(resultFromController));
})


/* PRODUCT: Soft Delete *
-------------------------------------------------------------*/
router.put("/:productId/archive", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const productId = req.params.productId;
	productController.archiveProduct(user, productId)
	.then(resultFromController => res.send(resultFromController));
})


/* PRODUCT: Hard Delete ^
-------------------------------------------------------------*/
router.delete("/:productId", (req, res) => {
	// const user = auth.decode(req.headers.authorization);
	const productId = req.params.productId;
	productController.deleteProduct(productId)
	.then(resultFromController => res.send(resultFromController));
})


module.exports = router;