const express = require("express");
const router = express.Router();
const orderController = require('../controllers/order')
const auth = require('./auth')



/* ORDER: Get All Orders of All Users (Admin only)
-------------------------------------------------------------*/
router.get("/", auth.verify, auth.isAdmin, (req, res) => {
	orderController.getOrders()
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Get All Orders of One Customer
-------------------------------------------------------------*/
router.get("/:customerId", (req, res) => {
	const user = req.params.userId;
	orderController.getMyOrders(user)
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Get One Order
-------------------------------------------------------------*/
router.get("/:orderId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const orderId = req.params.orderId;
	orderController.getOneOrder(user, orderId)
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Create Order (Customer only)
-------------------------------------------------------------*/
router.post("/:customerId", (req, res) => {
	// const user = auth.decode(req.headers.authorization);
	const user = req.params.customerId;
	let data = {
		name: req.body.name,
		productId: req.body.productId,
    	price: req.body.price,
    	quantity: req.body.quantity }
	orderController.createOrder(user, data)
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Update By Put *
-------------------------------------------------------------*/
router.put("/:orderId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const orderId = req.params.orderId;
	const data = {
		productId: req.body.productId,
    	price: req.body.price,
    	quantity: req.body.quantity }
	orderController.updateOrder(user, orderId, data)
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Add to Cart/Update By Patch (Customer only)
-------------------------------------------------------------*/
router.patch("/:orderId", auth.verify, auth.isCustomer, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const orderId = req.params.orderId;
	let data = {
		productId: req.body.productId,
    	price: req.body.price,
    	quantity: req.body.quantity }
	orderController.addToCart(user, orderId, data)
	.then(resultFromController => res.send(resultFromController));
});


/* ORDER: Archive/Soft Delete (Admin only)
-------------------------------------------------------------*/
router.put("/archive/:orderId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const orderId = req.params.orderId;
	orderController.archiveOrder(user, orderId)
	.then(resultFromController => res.send(resultFromController));
})


/* ORDER: Hard Delete (Admin only)
-------------------------------------------------------------*/
router.delete("/:orderId", auth.verify, auth.isAdmin, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const orderId = req.params.orderId;
	orderController.deleteOrder(user, orderId)
	.then(resultFromController => res.send(resultFromController));
})


/* ORDER: Export router
-------------------------------------------------------------*/
module.exports = router;