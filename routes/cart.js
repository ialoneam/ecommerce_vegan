const express = require("express")
const passport = require('passport')
const jwt = require('jsonwebtoken')
const cartController = require('../controllers/cart')
const auth = require('./auth')
const router = express.Router()



/* CART: Retrieve All
-------------------------------------------------------------*/
router.get("/all", (req, res, next) => {
	const user = auth.decode(req.headers.authorization);
	cartController.getAllInCart(user)
	.then(resultFromController => res.send(resultFromController))
	.catch(next)
})


/* CART: Retrieve Active ^
-------------------------------------------------------------*/
router.get("/:customerId", async (req, res) => {
	const user = req.params.customerId;
	cartController.getAllActive(user)
	.then(resultFromController => res.send(resultFromController))
})


/* CART: Retrieve One
-------------------------------------------------------------*/
router.get('/:userId', (req, res) => {
	let userId = req.params;
	console.log(userId);
	cartController.getCart(userId)
	.then(resultFromController => res.send(resultFromController))
})


/* CART: Add to Cart ^
-------------------------------------------------------------*/
router.put("/:customerId", async (req, res) => {
	const user = await req.params.customerId;
	cartController.addToCart(user, req.body)
	.then(resultFromController => res.send(resultFromController) )
})


/* CART: Update By Id
-------------------------------------------------------------*/
router.put("/:cartId", (req, res) => {
	// const user = auth.decode(req.headers.authorization);
	cartController.updateCart(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})


/* CART: Soft Delete ^
-------------------------------------------------------------*/
router.put("/:customerId/:itemId", async (req, res) => {
	const customerId = req.params.customerId;
	const itemId = req.params.itemId;

	console.log(customerId);
	console.log(itemId);
	cartController.archiveProduct(customerId, itemId)
	.then(resultFromController => res.send(resultFromController));
})


/* CART: Hard Delete
-------------------------------------------------------------*/
router.delete("/:customerId/:itemId", (req, res) => {
	// const user = auth.decode(req.headers.authorization);
	const customerId = req.params.customerId;
	const itemId = req.params.itemId;
	cartController.deleteProduct(cartId, productId)
	.then(resultFromController => res.send(resultFromController));
})


module.exports = router;