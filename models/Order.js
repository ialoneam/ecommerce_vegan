const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	customerId: {
		type: String,
		required: [true, "Customer ID is required"]
	},
	orders: [{
		productId: {
				type: String
		},
			price: {
				type: Number
		},
			quantity: {
				type: Number
		},
			subtotal: {
				type: Number
		}
	}],
	total: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: Date.now
	},
	isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model('Order', orderSchema);

