const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String
	},
	lastName: {
		type: String
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isActive: {
		type: Boolean,
		default: true
	},
	mobile: {
		type: String
	},
	address: {
    	type: String
    },
	facebookId: {
		type: String
	},
	googleId: {
		type: String
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	orders: [{
		orderId: {
			type: String
		},
		orderedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String
		}
	}],
	cart: [{
		productId: {type: String},
		name: {type: String},
		description: {type: String},
		price: {type: Number},
		quantity: {type: Number},
		subtotal: {type: Number},
		addedOn: {
			type: Date,
			default: Date.now }
	}]
})

module.exports = mongoose.model('User', userSchema)