const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	name: {
		type: String,
		unique: true,
		required: [true, 'Name field required']
	},
	price: {
		type: Number,
		required: [true, 'Price field is required']
	},
	description: {
		type: String,
		required: [true, 'Description field is required']
	},
	imgUrl: {
		type: String,
		default: ""
	},
	stocks: {
		type: Number,
		default: 0
	},
	reviews: [{
		customerId: {
			type: Number
		},
		comment: {
			type: String
		},
		rate: {
			type: Number
		}
	}],
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: Date.now
	}
})

module.exports = mongoose.model('Product', productSchema)