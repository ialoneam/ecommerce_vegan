const Cart = require('../models/Cart')
const User = require('../models/User')
const auth = require('../routes/auth')



/* CART: Retrieve All
-------------------------------------------------------------*/
module.exports.getAllInCart = (user) => {
	return Cart.find({}).then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* CART: Retrieve Active ^
-------------------------------------------------------------*/
module.exports.getAllActive = async (user) => {
	return Cart.find({customerId: user})
	.then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* CART: Retrieve One Product By ID
-------------------------------------------------------------*/
module.exports.getCart = (req) => {
	return Product.find({customerId: req})
	.then(result => {return result} )
}


/* CART: Add to Cart
-------------------------------------------------------------*/
module.exports.addToCart = async (user, data) => {

			let subtotal = 0;
			let productId = data.productId;
			let name = data.name;
			let description = data.description;
			let price = data.price;
			let quantity = 1;
			subtotal += (price * quantity);

	let isUserUpdated = await User.findById(user)
		.then(user => {

			user.cart.push({
				productId: productId,
				name: name,
				description: description,
				price: price,
				quantity: quantity,
				subtotal: subtotal
			});
			return user.save().then((user, error) =>{
				console.log(user);
				if(error) {
					return false;
				}
				else {
					return true;
				}
			})
		})

	let isCartUpdated = await Cart.find({customerId: user})
		.then(cart => {

			let total = cart[0].total;
			total += subtotal;

			cart[0].items.push({
				productId: productId,
				name: name,
				description: description,
				price: price,
				quantity: quantity,
				subtotal: subtotal 
			});
			cart[0].total = total;
			return cart[0].save().then((cart, error) => {
				console.log(cart);
				if(error) {
					return false;
				}
				else {
					return true;
				}			
			})		
		})

		if(isUserUpdated && isCartUpdated) {return true;} 
			else {return false;}
}


/* CART: Update By Put 
-------------------------------------------------------------*/
module.exports.updateCart = async (user, reqParams, reqBody) => {
	
	let updatedCart = {
		productId: reqBody.productId,
		price: reqBody.price,
		quantity: reqBody.quantity 
	}

	return Cart.findByIdAndUpdate(reqParams.customerId, updatedCart)
	.then((product, error) => {
		if (error) {return false} 
		else {return product} })
}


/* CART: Soft Delete
-------------------------------------------------------------*/
module.exports.archiveProduct = async (customerId, itemId) => {

	let isCartUpdated = await Cart.find({customerId: customerId})
		.then(cart => {
			console.log(cart);
			// let total = cart[0].total;
			// total -= subtotal;

			// let index = cart[0].items.indexOf(cartId);
			
			cart[0].isActive = false;
			return cart[0].save().then((cart, error) => {
				console.log(cart);
				if(error) {
					return false;
				}
				else {
					return true;
				}			
			})		
		})

		if(isCartUpdated) {return true;} 
			else {return false;}
}


/* CART: Hard Delete
-------------------------------------------------------------*/
module.exports.deleteProduct = async (user, productId) => {
	return Cart.deleteOne({_id: productId})
		.then((product, error) => {
			if (error) {return false} 
			else {return product} })
}