const Product = require('../models/Product')
const auth = require('../routes/auth')



/* PRODUCT: Retrieve All Products
-------------------------------------------------------------*/
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* PRODUCT: Retrieve All Active Products
-------------------------------------------------------------*/
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true})
	.then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* PRODUCT: Retrieve One Product By ID
-------------------------------------------------------------*/
module.exports.getProduct = async (reqParams) => {
	return Product.findById(reqParams)
	.then(result => {return result} )
}


/* PRODUCT: Check If Product Exists *
-------------------------------------------------------------*/
module.exports.checkProductExists = (body) => {
	return Product.find({name: body.name}).then(result => {
		if(result.length > 0) {return true} 
		else {return false} })
}


/* PRODUCT: Create Product
-------------------------------------------------------------*/
module.exports.addProduct = async (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		// imgUrl: reqBody.imgUrl 
	});

	return newProduct.save().then((product, error) => {
		if(error) {return false}
		else {return product} })
}


/* PRODUCT: Update By Put *
-------------------------------------------------------------*/
module.exports.updateProduct = async (productId, reqBody) => {
	
	console.log(productId);
	console.log(reqBody);
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		// imgUrl: reqBody.imgUrl 
	}

	return Product.findByIdAndUpdate(productId, updatedProduct)
	.then((product, error) => {
		if (error) {return false} 
		else {return true} })
	.catch(err => {console.log(err); return false;} );
}


/* PRODUCT: Soft Delete *
-------------------------------------------------------------*/
module.exports.archiveProduct = async (user, productId) => {
	return Product.findByIdAndUpdate(productId, {isActive: false})
	.then((product, error) => {
		if (error) {return false} 
		else {return product} })
}


/* PRODUCT: Hard Delete *
-------------------------------------------------------------*/
module.exports.deleteProduct = async (productId) => {
	return Product.deleteOne({_id: productId})
		.then((product, error) => {
			if (error) {return false} 
			else {return product} })
}