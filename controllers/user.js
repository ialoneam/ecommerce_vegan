const bcrypt = require('bcrypt')
const User = require('../models/User')
const Cart = require('../models/Cart')
const auth = require('../routes/auth')



/* USER: Check If Email Exists ^
-------------------------------------------------------------*/
module.exports.checkEmailExists = async (req) => {
	return User.find({email: req.email})
	.then(result => {
		if(result.length > 0){return true} 
		else {return false} })
	.catch(err => {console.log(err); return false;} ); }


/* USER: Create User ^
-------------------------------------------------------------*/
module.exports.createUser = async (req) => {

    let newUser = new User({
    	email: req.email,
    	password: bcrypt.hashSync(req.password, 10),
    	firstName: req.firstName,
    	lastName: req.lastName,
    	mobile: req.mobile,
    	cart: [],
    	orders: [], });

	return newUser.save().then((result, error) => {

		let newCart = new Cart({
			customerId: result._id,
			addedItems: [],
			total: 0,
		});
		newCart.save().then(result => console.log(result));

		if(error){return false;}
		else{return true;}
	}) }


/* USER: Login ^
-------------------------------------------------------------*/
module.exports.loginUser = async (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) { return false; }
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) } }
			else{ return false; } }})}


/* USER: Get Profile ^
-------------------------------------------------------------*/
module.exports.getProfile = (req) => {
	return User.findById(req).then(result => {
		if(result == null) {return false} 
		else { 	result.password = ""; 
				return result;} })}


/* USER: Get All *
-------------------------------------------------------------*/
module.exports.getUsers = () => {
	return User.find({}).then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* USER: Get All Active *
-------------------------------------------------------------*/
module.exports.getActiveUsers = () => {
	return User.find({isActive: true}).then(result => {
		if(result == null) {return false} 
		else {return result} })
}


/* USER: Update By Put *
-------------------------------------------------------------*/
module.exports.updateUser = async (user, reqBody, res) => {
	let updatedUser = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobile: reqBody.mobile,
		address: reqBody.address }

	
	return User.findByIdAndUpdate(user.id, updatedUser)
	.then((user, error) => {
		console.log(`updatedUser is ${user}`);
		if (error) {return false} 
		else {return user} })
}


/* USER: Update By Patch (Set User to Admin) *
-------------------------------------------------------------*/
module.exports.updateUserToAdmin = async (user, reqParams) => {
	return User.findByIdAndUpdate(reqParams, {isAdmin: true} )
	.then((user, error) => {
		if (error) {return false} 
		else {return user} })
}


/* USER: Update By Patch (Set Admin to Non-Admin) *
-------------------------------------------------------------*/
module.exports.updateAdminToUser = async (user, reqParams) => {
	return User.findByIdAndUpdate(reqParams, {isAdmin: false})
	.then((user, error) => {
		if (error) {return false} 
		else {return user} })
}


/* USER: Soft Delete *
-------------------------------------------------------------*/
module.exports.archiveUser = async (user, userId) => {
	return User.findByIdAndUpdate(userId, {isActive: false})
	.then((user, error) => {
		if (error) {return false} 
		else {return user} })
}


/* USER: Hard Delete *
-------------------------------------------------------------*/
module.exports.deleteUser = async (user, userId) => {
	return User.deleteOne({_id: userId})
	.then((user, error) => {
		if (error) {return false} 
		else {return user} })
}