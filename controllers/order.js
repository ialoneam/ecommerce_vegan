const bcrypt = require('bcrypt')
const Order = require('../models/Order')
const Product = require('../models/Product')
const User = require('../models/User')
const auth = require('../routes/auth')



/* ORDER: Get All Orders (Admin) *
-------------------------------------------------------------*/
module.exports.getOrders = () => {
	return Order.find({}).then(result => {
		if(result == null) {return false} 
		else {return result} })
	.catch(err => {console.log(err); return false;} );
}


/* ORDER: Get All Orders (Non-Admin) *
-------------------------------------------------------------*/
module.exports.getMyOrders = async (user) => {
	return Order.find({})
		.then(result => {
			if(result == null) {return false} 
			else {return result} })
		.catch(err => {console.log(err); return false;} );
}


/* ORDER: Get One Order *
-------------------------------------------------------------*/
module.exports.getOneOrder = async (user, orderId) => {
	return Order.findById(orderId).then(result => {
		console.log(result);

		if(result == null) {return false} 
		else {return result} })
}


/* ORDER: Create Order *
-------------------------------------------------------------*/
module.exports.createOrder = async (user, customerId, data) => {

	let subtotal = 0;
	let total = 0;
	let name = data.name;
	let productId = data.productId;
	let price = data.price;
	let quantity = data.quantity;
	subtotal += (price * quantity);
	total += subtotal;

	let newOrder = new Order({
		customerId: customerId,
		orders: {
			name: name,
			productId: productId,
			price: price,
			quantity: quantity,
			subtotal: subtotal },
		total: total })

	return newOrder.save().then((order, error) => {
		if(error) {return false}
		else {return order}})
	
	let isUserUpdated = await User.findById(customerId)
	.then(user => {user.orders.push({orderId: orderId});
	return user.save().then((user, error) => {
		if(error) {return false}
		else {return true} })})

	if(isUserUpdated && isOrderUpdated) {return order} 
	else {return false}
}



/* ORDER: Update By Put *
-------------------------------------------------------------*/
module.exports.updateOrder = async (user, orderId, data) => {
	return await Order.findById({_id: orderId}).then(order => {
		console.log(user);
		console.log(order);
		console.log(data);

		let customerId = order.customerId;
		let purchasedOn = order.purchasedOn;
		let isActive = order.isActive;
		let subtotal = 0;
		let total = order.total;
		let productId = data.productId;
		let price = data.price;
		let quantity = data.quantity;
		subtotal += (price * quantity);
		total += subtotal;

		order.customerId = customerId;
		order.purchasedOn = purchasedOn;
		order.isActive = isActive;

		order.orders.push({
			productId: productId,
			price: price,
			quantity: quantity,
			subtotal: subtotal });

		order.total = total;
	return order.save().then((order, error) => {
		console.log(order);
			if(error) {return false}
			else {return order} })})	
}


/* ORDER: Soft Delete *
-------------------------------------------------------------*/
module.exports.archiveOrder = async (user, orderId) => {
	return await Order.findByIdAndUpdate(orderId, {isActive: false})
	.then((order, error) => {
		if (error) {return false} 
		else {return order} })
}


/* ORDER: Hard Delete *
-------------------------------------------------------------*/
module.exports.deleteOrder = async (user, orderId) => {
	return await Order.deleteOne({_id: orderId})
	.then((order, error) => {
		if (error) {return false} 
		else {return order} })
}